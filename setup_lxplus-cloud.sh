#!/bin/bash

# ---------------------------------------------
# clean up and destroy password on script exit
# ---------------------------------------------
function cleanup() {
  rm -rf authdir/ /root/.docker/config.json
  export PACKTIVITY_AUTH_LOCATION=""
  export RECAST_REGISTRY_HOST=""
  export RECAST_REGISTRY_USERNAME=""
  export RECAST_REGISTRY_PASSWORD=""
  export YADAGE_SCHEMA_LOAD_TOKEN=""
  export RECAST_AUTH_USERNAME=""
  export RECAST_AUTH_PASSWORD=""
  export YADAGE_INIT_TOKEN=""
  eval $(recast auth destroy)
}
trap cleanup EXIT
trap cleanup SIGINT
trap cleanup SIGQUIT
trap cleanup SIGTSTP
# ---------------------------------------------

# on lxplus: setup RECAST
source ~recast/public/setup.sh || echo "Not on lxplus! Please connect to lxplus-cloud.cern.ch !"

# add analysis temporarily to catalogue
$(recast catalogue add $PWD)
recast catalogue ls
recast catalogue describe examples/monohbb
recast catalogue check examples/monohbb

