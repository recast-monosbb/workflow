## RECAST setup for mono-h(bb) analysis for Dark Higgs / mono-s(bb) reinterpretation

Code that runs the mono-h(bb) analysis code + statistical evaluation using ATLAS-CONF-2018-039 captured workflow.

RECAST documentation: https://recast-docs.web.cern.ch
Mono-s(bb) RECAST TWiki: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RECASTHadronicMonoSbb2019

### setup for production on lxplus-cloud

First, log in to the lxplus-cloud node
```
ssh lxplus-cloud.cern.ch
```

Then, setup the RECAST environment:
```
source setup_lxplus-cloud.sh
```

Now you can submit the mono-s(bb) signal grid to the Kubernetes worker nodes:
```
source submit_lxplus-cloud.sh [optional: specify monosbb|zp2hdm as argument, default: monosbb]
```

If you want to check the status of the jobs, you can execute:
```
source status_lxplus-cloud.sh [optional: specify monosbb|zp2hdm    as argument, default: monosbb]
```

To retrieve the results of the finished jobs (takes roughly 8 hours to run mono-s(bb) signals):
```
source retrieve_lxplus-cloud.sh [optional: specify monosbb|zp2hdm    as argument, default: monosbb]
```


Advanced features:
You can also directly check the status of kubernetes worker nodes:
```
# show avaliable nodes
kubectl get nodes
# show recent jobs
kubectl get pods
```

### setup for tests

To run a test please follow these instructions:
```
source run.sh
```

You will be prompted to enter username, password and gitlab token.

Maybe if you are running this on your laptop you need to install first a tool from https://github.com/recast-hep/recast-atlas and yadage.

to install recast-atlas:

```
git submodule init && git submodule update --recursive
cd recast-atlas
python setup.py
```

to install yadage (see here for details: https://github.com/yadage/tutorial):

```
pip install yadage[viz]
```

When using Singularity instead of Docker (experimental) it is required to provide credentials for the gitlab registry with environment variables:
```
# Singularity
export SINGULARITY_DOCKER_USERNAME=...
export SINGULARITY_DOCKER_PASSWORD=...
```

