#!/bin/bash
for f in ${1:-monosbb}/*.yml;do 
    echo ""
    echo "Submitting $f"
    INFOFILE=`echo $f | cut -d'/' -f 2`
    INFOFILE=${INFOFILE/.yml/.info}
    echo "info file for job monitoring: $INFOFILE"
    recast submit examples/monohbb $f --infofile $INFOFILE;
done
