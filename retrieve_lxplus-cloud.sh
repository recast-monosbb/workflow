#!/bin/bash

for f in ${1:-monosbb}/*.yml;do 
    echo ""
    echo "Checking $f"
    INFOFILE=`echo $f | cut -d'/' -f 2`
    INFOFILE=${INFOFILE/.yml/.info}
    recast retrieve --infofile $INFOFILE;
done
